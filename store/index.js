import Vuex from 'vuex'
const createStore = () => {
  return new Vuex.Store({
    state: {
      items: [],
      key: {}
    },
    mutations: {
      setItems (state, item) {
        state.items.push(item)
        setTimeout(
          function () {
            state.items.shift()
          }, 2000)
      },
      setKey (state, key) {
        state.key = []
        state.key = key
      },
      desactive (state, index) {
        state.items[index].isActive = false
      }
    }
  })
}
export default createStore
